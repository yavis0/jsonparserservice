import json
from flask import Flask
import requests

app = Flask(__name__)

@app.route('/api/v1/javascript', methods=['POST'])
def main():
  with open('package.json') as f:
    data = json.load(f);

  depJson = {};
  dependencies = {};
  depJson['name'] = data['name'];
  depJson['version'] = data['version'];
  depJson['url'] = data['repository']['url'];
  dependencies = data['dependencies'];
  depJson["dependencies"] = dependencies;

  depJson = json.dumps(depJson)
  print(depJson)
  res = requests.post('http://35.245.219.3:1337/dependencies', json=depJson);
  return 'WHAT'

if __name__ == "__main__":
  app.run(host="0.0.0.0")
